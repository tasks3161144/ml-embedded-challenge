import os
import cv2
import time
import numpy as np
from numba import cuda
import cupy as cp
import multiprocessing as mp
import pynvml
import psutil

pynvml.nvmlInit()
handle = pynvml.nvmlDeviceGetHandleByIndex(0)

# load mode
from ultralytics import YOLO
model = YOLO("yolov8n.pt")

# function for video processing
@cuda.jit
def process_video(video_path, output_path, model):
    cap = cv2.VideoCapture(video_path)
    frame_width = int(cap.get(3))
    frame_height = int(cap.get(4))
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    out = cv2.VideoWriter(output_path, fourcc, 30, (frame_width, frame_height))

    while True:
        ret, frame = cap.read()
        if not ret:
            break

        frame_gpu = cp.asarray(frame)  # transfer frame to GPU
        results = model(frame_gpu)
        
        for result in results:
            for r in result.boxes.data.tolist():
                x1, y1, x2, y2, score, class_id = r
                class_id = int(class_id)
                if class_id in ids:
                    x1 = int(x1) 
                    y1 = int(y1) 
                    x2 = int(x2) 
                    y2 = int(y2) 
                    cv2.rectangle(frame_gpu, (x1, y1), (x2, y2), (0, 255, 0), 3)
                else:
                    continue 
       

        annotated_frame = cp.asnumpy(frame_gpu)  # transfer frame back to CPU
        out.write(annotated_frame)

    cap.release()
    out.release()

# main function
def main():
    
    video_paths = ["/content/drive/MyDrive/Interview/27991977-b5ac-4d00-a561-68.mp4", "/content/drive/MyDrive/Interview/5923b832-0e36-4f7e-993b-59.mp4","/content/drive/MyDrive/Interview/d126350b-d57a-4e2c-817f-e2.mp4","/content/drive/MyDrive/Interview/dd6d217c-7c51-4f19-b7c3-8d.mp4"]
   

    # limit CPU usage 
    while True:
        cpu_percent = psutil.cpu_percent()
        if cpu_percent < 20:  
            break
        time.sleep(1)

    with mp.Pool(4) as pool:
            pool.map(process_video, video_paths)

if __name__ == '__main__':
    
    mp.freeze_support()
    
    start_time = time.time()
    
    main()
    
    end_time = time.time()
    execution_time = end_time - start_time
    print(f"Execution time (Parallel): {execution_time} seconds")
    
    info = pynvml.nvmlDeviceGetMemoryInfo(handle)
    print(f"GPU Memory Total: {info.total / (1024 ** 2)} MB")
    print(f"GPU Memory Free: {info.free / (1024 ** 2)} MB")
    print(f"GPU Memory Used: {info.used / (1024 ** 2)} MB")

    pynvml.nvmlShutdown()
