# Video Object Detection with YOLOv8n

This task is aimed at processing videos in parallel, detecting objects, and generating annotated videos with detections using the YOLOv8n model. The implementation leverages GPU acceleration for faster processing.

## Prerequisites

- Python 3.7 or higher
- Required Python packages (install using `pip`):
  - `opencv-python`
  - `numpy`
  - `numba`
  - `cupy`
  - `ultralytics` (for YOLO model)
  - `multiprocessing`

- A compatible GPU with CUDA support 
- YOLOv8n model file (`yolov8n.pt`)
