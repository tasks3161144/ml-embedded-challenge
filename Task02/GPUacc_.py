import multiprocessing as mp # import library for multiprocessing
import cv2
import os
from ultralytics import YOLO
import time
#import tensorflow as tf
import psutil
from numba import jit, cuda


# function to process the video
@jit(target_backend='cuda')	
def process_video(video):
    
    #print(len(tf.config.list_physical_devices('GPU')))
    ids= [0,1,2,3,4,5,6,7,8] 
    
    save_path = 'C:/Users/OSHADHI/Desktop/MeShEs/outputs'
    save_file_path = os.path.join(save_path,video)   # path to save the video with same name 
    
    model = YOLO("yolov8n.pt") # load yolov8n model 
    
    cap = cv2.VideoCapture(video)
    ret,frame = cap.read()                                # read video
    
    cap_out = cv2.VideoWriter(save_file_path,cv2.VideoWriter_fourcc(*'MP4V'), cap.get(cv2.CAP_PROP_FPS),(frame.shape[1],frame.shape[0]))  # set save video file features according to the input video file  
    # capture objects in the video
    while ret:

        results = model(frame)

        for result in results:
           
            for r in result.boxes.data.tolist():

                x1, y1, x2, y2, score, class_id =r
                class_id = int (class_id)
                if class_id in ids :
                    x1 = int(x1) 
                    y1 = int(y1) 
                    x2 = int(x2) 
                    y2 = int(y2) 
                    cv2.rectangle(frame,(x1,y1),(x2,y2), (0,255,0), 3)
                else:
                    continue      
        
        cap_out.write(frame)  # save output video
        ret,frame = cap.read()

    cap_out.release()
    cap.release()


if __name__ == '__main__':
    
    start_time = time.time()
    
    mp.freeze_support()
    
    video_paths = ["todos_videos_original_79b8610c-40fc-4837-baa1-b8.mp4", "todos_videos_original_5859228f-4580-477d-bef0-f0.mp4", "todos_videos_processed_bdd981b3-13a3-4f49-af1f-38d7b842abc8.mp4", "todos_videos_processed_cc35ad83-e64d-455e-927c-7cb842a6f916.mp4"]
    
    while True:
        
        cpu_percent = psutil.cpu_percent()
        
        if cpu_percent < 20:  # Adjust this value to set the desired CPU usage limit
            break
        time.sleep(1)  # Sleep to avoid excessive checking
       
    with mp.Pool(4) as pool:
            pool.map(process_video, video_paths)
    
    #tf.debugging.set_log_device_placement(True)
    
    end_time = time.time()
    execution_time = end_time - start_time        
    print(f"Execution time (Parallel): {execution_time} seconds") 
