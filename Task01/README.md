# Video Processing 

## Overview

This video processing code is designed to process videos one by one, perform object detection, and generate annotated videos with object detections. The code uses a selected object detection model to identify persons and vehicles in the input videos.

## Features

- Process videos sequentially
- Detect and annotate objects in videos
- Identify persons and vehicles
- Supports various video formats (MP4, AVI, MOV)

## Object Detection Model

The code uses the YOLO (You Only Look Once) object detection model, specifically YOLOv8n, for identifying persons and vehicles in videos. YOLOv8n is chosen for its real-time object detection capabilities and high accuracy.

## Requirements

- Python 3.x
- OpenCV (for video processing)
- Ultralytics (for YOLO model)
- Pre-trained YOLOv8n model (yolov8n.pt)

## Outputs

[Google Drive Link](https://drive.google.com/drive/folders/1ZSZfnTiv_v-tvis7VqoYjoYqlq1Lnh6u)

