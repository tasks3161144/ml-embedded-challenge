import cv2
from ultralytics import YOLO

save_path = "/content/drive/MyDrive/Interview/outputs/person_only" 
video = "/content/drive/MyDrive/Interview/alarms/person_only/1a865b94-1617-4aa0-a39d-6b.mp4"
model = YOLO("yolov8n.pt") # load yolov8n model

#indexes for vehicles   
ids = [0,1,2,3,4,5,6,7,8]       
cap = cv2.VideoCapture(video)
ret,frame = cap.read()
cap_out = cv2.VideoWriter(save_path,cv2.VideoWriter_fourcc(*'MP4V'), cap.get(cv2.CAP_PROP_FPS),(frame.shape[1],frame.shape[0]))  # set save video file features according to the input video file  
while ret:

    results = model(frame)

    for result in results:
           
        for r in result.boxes.data.tolist():

            x1, y1, x2, y2, score, class_id =r
            class_id = int (class_id)
            if class_id in ids :
                x1 = int(x1) 
                y1 = int(y1) 
                x2 = int(x2) 
                y2 = int(y2) 
                cv2.rectangle(frame,(x1,y1),(x2,y2), (0,255,0), 3)
            else:
                continue      
    
    cap_out.write(frame)  # save output video
    ret,frame = cap.read()

cap_out.release()
cap.release()
