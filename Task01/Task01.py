import os
import cv2
from ultralytics import YOLO
import psutil
import time

start_time = time.time()

def detect(folder_path,save_path, ids):

    video_files = [f for f in os.listdir(folder_path) if f.endswith((".mp4", ".avi", ".mov"))]  #Capture only video files
    # go through each and every video file
    for video_file in video_files:

        video_path = os.path.join(folder_path, video_file)    # video file path 
        save_file_path = os.path.join(save_path,video_file)   # path to save the video with same name  
        
        cap = cv2.VideoCapture(video_path)
        ret,frame = cap.read()                                # read video
        
        cap_out = cv2.VideoWriter(save_file_path,cv2.VideoWriter_fourcc(*'MP4V'), cap.get(cv2.CAP_PROP_FPS),(frame.shape[1],frame.shape[0]))  # set save video file features according to the input video file  
        
        # capture objects in the video
        while ret:

            results = model(frame)

            for result in results:
           
                for r in result.boxes.data.tolist():

                    x1, y1, x2, y2, score, class_id =r
                    class_id = int (class_id)
                    if class_id in ids :
                        x1 = int(x1) 
                        y1 = int(y1) 
                        x2 = int(x2) 
                        y2 = int(y2) 
                        cv2.rectangle(frame,(x1,y1),(x2,y2), (0,255,0), 3)
                    else:
                        continue      
    
            cap_out.write(frame)  # save output video
            ret,frame = cap.read()

    cap_out.release()
    cap.release()

model = YOLO("yolov8n.pt") # load yolov8n model

person = [0]                           #index for person                                  
vehicles = [1,2,3,4,5,6,7,8]           #indexes for vehicles   
both = [0,1,2,3,4,5,6,7,8]             #indexes for both person and vehicle 

folder_path = "/content/drive/MyDrive/Interview/alarms/person_only"         #path for "person_only" folder
save_path = "/content/drive/MyDrive/Interview/outputs/person_only"          #path for folder to save outputs of videos in the "person_only" folder
detect(folder_path,save_path, person)

folder_path = "/content/drive/MyDrive/Interview/alarms/vehicle_only"        #path for "vehicle_only" folder
save_path = "/content/drive/MyDrive/Interview/outputs/vehicle_only"         #path for folder to save outputs of videos in the "vehicle_only" folder
detect(folder_path,save_path, vehicles)

folder_path = "/content/drive/MyDrive/Interview/alarms/person_vehicle_movement"                     #path for "person_vehicle_movement" folder
save_path = "/content/drive/MyDrive/Interview/outputs/person_vehicle_movement"                      #path for folder to save outputs of videos in the "person_vehicle_movement" folder 
detect(folder_path,save_path, both)

folder_path = "/content/drive/MyDrive/Interview/alarms/spacial case with some false detection"      #path for "spacial case with some false detection" folder
save_path = "/content/drive/MyDrive/Interview/outputs/spacial case with some false detection"       #path for folder to save outputs of videos in the "spacial case with some false detection" folder
detect(folder_path,save_path, both)   

# Evaluate Memory

memory_info = psutil.virtual_memory()
tot = memory_info.total / (1024 ** 3)
avlbl = memory_info.available / (1024 ** 3)
print(f"Total Memory: {tot} Gb")
print(f"Available Memory: {avlbl} Gb")

# Evaluate Time

end_time = time.time()
execution_time = end_time - start_time
print(f"Execution time: {execution_time} seconds")
